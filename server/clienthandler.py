import threading
import os
import pickle
from message import *
import saves as s


BUFFER_SIZE = 1024*4
lock = threading.Lock()


class ClientService(threading.Thread):
	def __init__(self, connection, address, ctrl):
		threading.Thread.__init__(self, target=self.run)
		self.connection = connection
		self.address = address
		self.algorithm_updated = False
		self.ctrl = ctrl
		self.daemon = True
		self.is_ready = False
		self.is_connected = True

	def print_add(self, *args):
		print(self.address[1],*args)

	def sendData(self, data):
		with lock:
			try:
				self.connection.sendall(pickle.dumps(data))
			except:
				self.print_add("sending failed", filename)
				self.is_connected = False

	def getData(self):
		data = pickle.loads(self.connection.recv(BUFFER_SIZE))
		return data.command(), data.container()


	def sendFile(self, filename):
		try:
			self.sendData(Message("file", None))
			filesize = os.path.getsize(filename)
			print(filename, filesize)

			self.sendData(Message(filename,filesize))

			with open(filename, "rb") as f:
				while True:
					bytes_read = f.read(BUFFER_SIZE)
					if not bytes_read:
						with lock:
							self.connection.sendall('eof'.encode())
						break
					with lock:
						self.connection.sendall(bytes_read)
			dcmd, _ = self.getData()
			self.print_add(dcmd + filename)
		except:
			self.print_add("sending failed", filename)
			self.is_connected = False


	def send_algorithm(self):
		cmd, ctr = "name", self.ctrl.algorithm_name
		try:
			self.sendData(Message(cmd,ctr))
			dcmd, dctr = self.getData()
		except:
			self.print_add("client lost")
			self.is_connected = False
			return

		if dcmd == "name_written":
			self.print_add("name written")
		elif dcmd == "name_writing_error":
			self.print_add("could not write name")
			self.is_connected = False
			return

		for file in s.listDir(self.ctrl.resources_folder):
			if file[-3:] == ".py":
				self.sendFile(self.ctrl.resources_folder + file)

		self.sendData(Message("import",None))
		dcmd, _ = self.getData()
		self.is_ready = True

	def disconnect(self):
		try:
			self.sendData(Message("exit"))
		except:
			self.print_add("client lost after finish")
		self.is_connected = False

	def evolution_step(self):
		dcmd = None
		dctr = None
		cmd = None
		ctr = None
		individual = None
		idx = None

		while not all(self.ctrl.finishTracker):
			if not individual:
				for i in range(len(self.ctrl.Algorithm.population)):
					if self.ctrl.populationTracker[i]:
						self.ctrl.populationTracker[i] = False
						individual = self.ctrl.Algorithm.population[i]
					if individual:
						idx = i
						individual.score = 0
						cmd = "calculate"
						ctr = [individual]
						break
			if not individual:
				cmd = "wait"
				
			try:
				self.sendData(Message(cmd,ctr))
			except:
				print("couldnt send")
				self.is_connected = False
				break
			

			try:
				dcmd, dctr = self.getData()
			except:
				print("couldnt get")
				self.is_connected = False
				break

			if dcmd == "result":
				self.print_add("Score: ", dctr)
				individual.score = dctr
				self.ctrl.finishTracker[idx] = True 
				individual = None
				cmd = None
				ctr = None

			elif dcmd == "wait":
				self.print_add("Waitiing...")
				break

			else:
				self.print_add("Invalid command receieved")
				response = Message("print", "Command not found")

		if idx != None:
			if not self.ctrl.finishTracker[idx] and not self.ctrl.populationTracker[idx]:
				self.ctrl.populationTracker[idx] = True
				self.print_add("did not finish, rolling back...")



	def run(self):
		while self.is_connected:

			if not self.is_ready:				
				if self.ctrl.task == "EVOLVE":
					self.evolution_step()
					self.is_ready = True

				elif self.ctrl.task == "SEND":
					if self.ctrl.experiment_changed:
						self.algorithm_updated = False
					if not self.algorithm_updated:
						self.send_algorithm()
						self.algorithm_updated = True
					
					self.is_ready = True

		self.connection.close()
		self.ctrl.Server.clients.remove(self)
		self.print_add("client is off")



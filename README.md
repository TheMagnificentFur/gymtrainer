# GymTrainer

## Overlook

## Requirements

For running this software, the installation of the following dependencies is mandatory.

Install the latest Python version, pip, and tkinter.

Ubuntu:

```
sudo apt update
sudo apt install python
sudo apt install python-pip
sudo apt install python-tk
```

Archlinux:

```
sudo pacman -Syu python python-pip tk
```

Install the following python libraries using pip:
	Pytorch
	OpenAI Gym
	thread6
	NumPy
	PySimpleGui
	pickle-mixin
	importlib
	sockets
    matplotlib
```
pip3 install torch gym thread6 numpy PySimpleGUI pickle-mixin importlib sockets matplotlib
```

## Installation

There's no need to install it yet, just clone and run via Python3

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

## Project status

p.random.choice([0, 1], size=x.shape,
											p=((1 - self.mr), self.mr)).astype(bool)
					x[mask] = np.random.rand(*x[mask].shape)-0.5
					layer.weight = torch.nn.Parameter(torch.from_numpy(x))
					if np.random.choice([0, 1],
										p=((1 - self.bmr), self.bmr)).astype(bool):
						layer.bias.data.fill_(np.random.rand()-0.5)

	def crossover(self):
		while len(self.population)<self.size:
			parents = np.random.choice(self.population[:int(self.size*self.cb)], 2)
			children = [Net(self.nnStructure), Net(self.nnStructure)]
			for tp0,tp1,tc0,tc1 in zip(parents[0].model, parents[1].model,
									children[0].model, children[1].model):
				if type(tp1) == nn.Linear:
					p0, p1 = tp0.weight.numpy(), tp1.weight.numpy()
					c0, c1 = tc0.weight.numpy(), tc1.weight.numpy()

					mask = np.random.choice([0, 1], size=p0.shape).astype(bool)

					c0[mask] = np.copy(p0[mask])
					c1[mask] = np.copy(p1[mask])
					c0[~mask] = np.copy(p1[~mask])
					c1[~mask] = np.copy(p0[~mask])

					tc0.weight = torch.nn.Parameter(torch.from_numpy(c0))
					tc1.weight = torch.nn.Parameter(torch.from_numpy(c1))

					if np.random.choice([0,1]).astype(bool):
						tc0.bias = tp0.bias
						tc1.bias = tp1.bias
					else:
						tc0.bias = tp1.bias
						tc1.bias = tp0.bias

			self.population.extend(children)

	def evaluation(self):
		for individual in self.population:
			individual.score = 0
			observation = self.env.reset()
			for _ in range(self.st):
				action = self.decide_action(individual, observation)
				observation, reward, done, _ = self.env.step(action)
				individual.score += reward
				if done:
					break
		self.env.close()

	def simulate(self):
		observation = self.env.reset()
		for _ in range(self.st):
			self.env.render()
			action = self.decide_action(self.best, observation)
			observation, _, done, _ = self.env.step(action)
			if done:
				break
		self.env.close()

	def evolve(self, epoch):
		for i in range(epoch):
			print("Evolution step: {}.".format(i+1))
			self.evaluation()
			self.selection()
			self.crossover()
			self.mutation()

	def client_evolve(self):
		self.evaluation()

	def server_evolve(self):
		self.selection()
		self.crossover()
		self.mutation()




if __name__ == "__main__":
	GA = Algorithm()
	GA.evolve(10)
	input("sim?")
	GA.simulate()
